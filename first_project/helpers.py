"""
All helper functions go here
"""


def inc(x: int):
    """ Returns argument incremented by one """
    return x + 1
