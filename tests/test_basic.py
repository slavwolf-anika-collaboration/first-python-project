"""
Tests go here
"""
from first_project.helpers import inc
import unittest


class TestIncMethod(unittest.TestCase):

    def test_inc(self):
        self.assertEqual(inc(4), 5)


if __name__ == '__main__':
    unittest.main()
