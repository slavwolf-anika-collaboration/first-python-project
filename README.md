# Anika's first project

## Project structure
```
├── first_project
│   ├── __init__.py
│   ├── helpers.py
│   └── project.py
├── scripts
│   └── script.sh
├── tests
│   ├── __init__.py
│   └── test_basic.py
├── LICENSE
├── README.md
└── requirements.txt

```

| Location           | Purpose                                         |
|:-------------------|:------------------------------------------------|
| ./first_project/   | Your project code.                              |
| ./scripts/         | Various scripts that are not written in Python. |
| ./tests/           | Package integration and unit tests.             |
| ./LICENCE          | Your project license.                           |
| ./README.md        | y                                               |
| ./requirements.txt | Development dependencies.                       |

## Rules

- All changes should be done on branch and merged into `master` branch via Pull Requests.
- Code should have docstrings with descriptions.
- Code must follow PEP8
- All new features should be covered by tests.

## Tips

How to run tests:
`pytest tests/test_basic.py`